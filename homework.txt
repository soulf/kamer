RESUME OF VERSION CONTROL
version control is a software tool that tracks changes to files or documents overtime. allowing user to manage and keep track of different versions of the same file
BENEFITS OF VERSION CONTROL
 the version control is commonly used in software development but can also be use for other type of file like spreadsheet, pictures and text document.
 the version control enable multiple user to collaborate on a project, keep track of changes and revert to earlier versions if necessary.
 DISTRIBUTED VERSION CONTROL
  the distributed version control has no point of failure because changes are made in the duplicated branch which will be test and approved before the merge.
  CENTRALIZED VERSION CONTROL
  the  centralized version control has some point of failure because changes are made in the only main branch.